function showDate() //displays date last modified on the bottom of every html page
{
    var date = document.lastModified;
    document.getElementById("footer").innerHTML = "Last modified: " + date;
}
function tabulate() //concatenates an order summary based on values submitted from form. Outputs to textarea.
{
    //quantity of each color ordered
    var blueq = parseInt(document.punch_card_orders.blue.value);
    var yellowq = parseInt(document.punch_card_orders.yellow.value);
    var whiteq = parseInt(document.punch_card_orders.white.value);

    //if any quantities are negative, throw alert and return
    if (blueq < 0 || yellowq < 0 || whiteq < 0)
    {
	alert("Please make sure to include only positive integers.");
	return;
    }
    
    //Compute subtotals by multiplying price (hardcoded) and quantity.
    var bluesub = blueq * 5;
    var yellowsub = yellowq * 6;
    var whitesub = whiteq * 8;

    //total = sum of subtotals
    var total = bluesub + yellowsub + whitesub; 
    
    //Output:
    var orderConfirmation = "Subtotal: \n";
    //If he or she ordered any blue packages.
    if (blueq > 0)
    {
	orderConfirmation = orderConfirmation + "Packs of 50 Blue Punch Cards: " + blueq + "\n";
	orderConfirmation = orderConfirmation + "Subtotal: $" + bluesub + "\n";
    }
    
    //If he or she ordered any yellow packages.
    if (yellowq > 0)
    {
	orderConfirmation = orderConfirmation + "Packs of 50 Yellow Punch Cards: " + yellowq + "\n";
	orderConfirmation = orderConfirmation + "Subtotal: $" + yellowsub + "\n";
    }
    
    //If he or she ordered any white packages.
    if (whiteq > 0)
    {
	orderConfirmation = orderConfirmation + "Packs of 200 White Punch Cards: " + whiteq + "\n";
	orderConfirmation = orderConfirmation + "Subtotal: $" + whitesub + "\n";
    }
    
    //Draw line, give total.
    orderConfirmation = orderConfirmation + "____________________" + "\n" + "Total: $" + total; + "\n";
    
    //Output to text area.
    document.getElementById("outputbox").value = orderConfirmation;
}

function clear()
{
    document.getElementById("order_confirmation").innerHTML = "";
    alert("cleared");
}
